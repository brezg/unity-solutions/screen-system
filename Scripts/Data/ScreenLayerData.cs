﻿using System;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Unity.Collections;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;


namespace Brezg.ScreenSystem
{
	[Serializable]
	public class ScreenLayerData
	{
		[Required]
		public Transform Root;

		[CanBeNull]
		[SerializeField, SceneObjectsOnly]
		public BaseScreen CurrentScreen;

		public bool IsLayerEmpty => CurrentScreen == null;


		public BaseScreen SpawnScreen(BaseScreen screenPrefab)
		{
			if (!IsLayerEmpty)
				throw new Exception($"Layer is not empty: {Root.name}");
			
			#if UNITY_EDITOR
			if (Application.isPlaying)
				CurrentScreen = Object.Instantiate(screenPrefab, Root);
			else
				CurrentScreen = ((BaseScreen) PrefabUtility.InstantiatePrefab(screenPrefab, Root));
			#else
			CurrentScreen = Object.Instantiate(screenPrefab, Root);
			#endif
			
			Debug.Log(CurrentScreen);

			return CurrentScreen;
		}

		[Button]
		public void ClearLayer()
		{
			if (IsLayerEmpty)
				return;
			
			if (Application.isPlaying)
				Object.Destroy(CurrentScreen.gameObject);
			else
				Object.DestroyImmediate(CurrentScreen.gameObject);

			CurrentScreen = null;
		}
	}
}
