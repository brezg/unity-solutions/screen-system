﻿namespace Brezg.ScreenSystem
{
	public enum ScreenLayer
	{
		Background,
		Screen,
		Overlay,
		Window,
		SubWindow,
		Notifications,
		Menu,
		SubMenu,
		Tooltip,
	}
}
