﻿using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;


namespace Brezg.ScreenSystem
{
	public abstract class BaseScreen : SerializedMonoBehaviour
	{
		[SerializeField]
		private ScreenLayer _layer;
		
		public ScreenLayer Layer => _layer;

		
		public virtual async UniTask OnShow()
		{
			await UniTask.CompletedTask;
		}
		
		public virtual async UniTask OnHide()
		{
			await UniTask.CompletedTask;
		}
	}
}
