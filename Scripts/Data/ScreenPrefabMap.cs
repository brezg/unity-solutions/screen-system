﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;


namespace Brezg.ScreenSystem
{
	[CreateAssetMenu(fileName = "ScreenPrefabMap", menuName = "Brezg/Config/Префабы экранов")]
	public class ScreenPrefabMap : SerializedScriptableObject
	{
		[SerializeField, AssetsOnly, Required]
		private List<BaseScreen> _screens = new List<BaseScreen>();

		
		public TScreen GetPrefab<TScreen>()
			where TScreen : BaseScreen
		{
			foreach (var baseScreen in _screens)
				if (baseScreen is TScreen screen)
					return screen;

			throw new IndexOutOfRangeException($"Can't find view of type {typeof(TScreen)}");
		}
	}
}
