﻿using System;
using Brezg.Serialization;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;


namespace Brezg.ScreenSystem
{
	public class ScreenManager : SerializedMonoBehaviour
	{
		[SerializeField, AssetsOnly, Required]
		private ScreenPrefabMap _prefabs;
		
		
		[SerializeField, ChildGameObjectsOnly, Required]
		private UnityDictionary<ScreenLayer, ScreenLayerData> _layers = new UnityDictionary<ScreenLayer, ScreenLayerData>();

		
		[Button]
		public async UniTask<TScreen> ShowScreen<TScreen>()
			where TScreen : BaseScreen
		{
			Debug.Log($"Showing screen {typeof(TScreen)}");
			
			var screenPrefab = _prefabs.GetPrefab<TScreen>();

			if (screenPrefab is null)
				throw new NullReferenceException($"No {typeof(TScreen)} screen in ScreenPrefabMap!");
			
			var layer = _layers[screenPrefab.Layer];
			
			if (!layer.IsLayerEmpty)
				ClearLayer(layer);

			var screenInstance = layer.SpawnScreen(screenPrefab);
			await screenInstance.OnShow();

			return (TScreen) screenInstance;
		}
		

		[Button]
		public async UniTask HideScreen<TScreen>()
			where TScreen : BaseScreen
		{
			Debug.Log($"Hiding screen {typeof(TScreen)}");
			
			var screenPrefab = _prefabs.GetPrefab<TScreen>();
			var layer = _layers[screenPrefab.Layer];

			if (layer.IsLayerEmpty)
				throw new NullReferenceException($"Экран {typeof(TScreen)} отсутствует на сцене");
			
			ClearLayer(layer);
		}


		private async void ClearLayer(ScreenLayerData layer)
		{
			await layer.CurrentScreen.OnHide();
			layer.ClearLayer();
		}

		public TScreen GetScreen<TScreen>() 
			where TScreen : BaseScreen
		{
			var screenPrefab = _prefabs.GetPrefab<TScreen>();
			var layer = _layers[screenPrefab.Layer];

			return (TScreen) layer.CurrentScreen;
		}
		
	}
}
